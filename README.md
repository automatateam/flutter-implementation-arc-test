# Flutter code review

L'objectif de ce test est de faire une code review du dossier .lib.

Il faut que, avec tous les correctifs que vous pourriez apporter, le code soit le plus parfait possible à vos yeux.
Il est important de remonter tout problème d'architecture, décomposition en composants, 