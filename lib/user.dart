class User {
  String id;
  String createdAt;
  String name;
  String avatar;

  User(this.id, this.createdAt, this.name, this.avatar);
}
