import 'package:flutter/material.dart';
import 'package:flutter_interview/home_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 50), // Because we dont want the app bar behind the iPhone notch
                Container(
                  height: 50,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      'User App',
                      style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                HomePage(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
