import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 50), // Because we dont want the app bar behind the iPhone notch
                Container(
                  height: 50,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      'User App',
                      style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                HomePage(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

const apiPath = "https://61daf79f4593510017aff742.mockapi.io/User";

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getUsers(),
        builder: (context, snapshot) {
          return SingleChildScrollView(
            child: Column(
              children: _createUserWidget(snapshot.data as List),
            ),
          );
        });
  }

  List<Widget> _createUserWidget(List<dynamic>? users) {
    if (users == null) {
      return [Text('No user found.')];
    }
    var widgets = <Widget>[];
    var index = 0;

    users.forEach((element) {
      final user = element as User;
      final usernameReduced = user.name.length > 20 ? '${user.name.substring(0, 20)}...' : user.name;
      var widget = Row(
        children: [
          SizedBox(width: 10),
          Column(
            children: [
              Container(
                  width: 200,
                  child: Text(
                    usernameReduced,
                    style: TextStyle(fontSize: 14, color: Colors.black87, fontWeight: FontWeight.w500),
                  )),
              SizedBox(height: 10),
              Text(user.createdAt, style: TextStyle(fontSize: 12, color: Colors.black45, fontStyle: FontStyle.italic)),
              if (index < users.length - 1)
                Container(
                  height: 16,
                )
            ],
          ), // Column
          Flexible(
            child: Container(),
          ),
          Image.network(user.avatar, width: 30, height: 30),
          SizedBox(width: 10),
        ],
      ); // Row

      widgets.add(widget);
    });

    return widgets;
  }

  _getUsers() {
    var url = Uri.parse('https://61daf79f4593510017aff742.mockapi.io/User');
    return http.get(url).then((response) {
      var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as List;
      var users = [];

      decodedResponse.forEach((element) {
        var map = element as Map<dynamic, dynamic>;
        users.add(User(map['id'], map['createdAt'], map['name'], map['avatar']));
      });

      print(decodedResponse);
      return users;
    });
  }
}

class User {
  String id;
  String createdAt;
  String name;
  String avatar;

  User(this.id, this.createdAt, this.name, this.avatar);
}
